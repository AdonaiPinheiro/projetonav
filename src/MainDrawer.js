import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

import TabNav from './TabNav/MainTab';

const MainDrawer = createDrawerNavigator({
    TabNav:{
        screen:TabNav,
        navigationOptions:{
            drawerLabel:'Alguma coisa',
            drawerIcon: ({ tintColor }) => <Icon name='user-cog' size={17} />,
        }
    }
});

export default MainDrawer;