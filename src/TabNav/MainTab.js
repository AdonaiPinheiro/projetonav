import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Home from '../Pages/Home';
import Perfil from '../Pages/Perfil';

const MainTab = createBottomTabNavigator({
    Home:{
        screen:Home,
        navigationOptions:{
            tabBarLabel:"Adonai",
            tabBarIcon: ({ tintColor }) => (
                <Icon
                    name="microchip"
                    size={30}
                    color={tintColor} />
            )
        }
    },
    Perfil:{
        screen:Perfil
    }
}, {
    tabBarOptions:{
        labelStyle:{
            justifyContent:'center',
            alignItems:'center',
            fontSize:18
        }
    }
});

export default MainTab;