import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import MainDrawer from './src/MainDrawer';

const AppNavigator = createStackNavigator({
  MainDrawer:{
    screen:MainDrawer
  }
}, {
  defaultNavigationOptions:{
    header:null
  }
});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;